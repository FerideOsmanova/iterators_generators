class FlatIterator:

	def __init__(self, list: list):
		self.main_list = list


	def __iter__(self):
		self.main_list_cursor = 0
		self.current_list_cursor = -1
		return self


	def __next__(self): 
		self.current_list_cursor += 1
	
		if self.current_list_cursor  == len(self.main_list[self.main_list_cursor]):         
			
			self.current_list_cursor  = 0
			self.main_list_cursor += 1
			
		if self.main_list_cursor == len(self.main_list):
			raise StopIteration

		return self.main_list[self.main_list_cursor][self.current_list_cursor]




def flat_generator(my_list):
    my_list = [ j for i in my_list for j in i]
    count = 0
    while count < len(my_list):
        yield my_list[count]
        count += 1

nested_list = [
	['a', 'b', 'c'],
	['d', 'e', 'f', 'h', False],
	[1, 2, None, 'None1'],
    [10, 20, None, 'None20'],
]

# for item in FlatIterator(nested_list):
# 	print(item)


for item in  flat_generator(nested_list):
	print(item)
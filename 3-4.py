
class NestedIterator:

    def __init__(self, list) -> None:
        self.list = list
        
    def __iter__(self):
        self.iter_list = [iter(self.list)]
        return self
    
    def __next__(self):

        while self.iter_list:

            try:
                value  = next(self.iter_list[-1])
            except StopIteration:                
                self.iter_list.pop()
                continue

            if isinstance(value, list):
                self.iter_list.append(iter(value))
                continue
            return value

        raise StopIteration


def NestedGenerator(my_list):
    iter_list = [iter(my_list)]

    while iter_list:
        try:
            value  = next(iter_list[-1])
        except StopIteration:                
            iter_list.pop()
            continue

        if isinstance(value, list):
            iter_list.append(iter(value))
            continue
        else:
            yield value

nested_list = [
	['a',[10, 20, True, 'None20'], 'b', 'c', [[11, 22, 'None11', 'None22'], 'd', 'e', 'f', 'h', False]],
	['d', 'e', 'f', 'h', False, ['a', 'b', 'c']],
	[1, 2, None, 'None1'],
    [10, 20, True, 'None20'],
]


# for item in NestedIterator(nested_list):
# 	print('item --', item)

# for item in NestedGenerator(nested_list):
# 	print('item --', item)